import time

from flask import Flask
from flask import request

app = Flask(__name__)

def ask_service2():
    return

@app.route('/')
def hello():
    message = f"Hello from {request.environ['REMOTE_ADDR']}:{request.environ['REMOTE_PORT']} to {request.host}"
    return message

if __name__ == '__main__':
    app.run(host='0.0.0.0')