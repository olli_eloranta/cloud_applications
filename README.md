# TIE-23536 Cloud Applications

This repository contains the programs in the course's weekly exercises.

* Exercise 4.1

This exercise can be run simply with the command 'docker-compose up' and 
the results can be seen by visiting localhost:8001. Tested in Ubuntu 18.04 in 
VirtualBox.